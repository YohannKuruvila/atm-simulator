﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMSimulator
{
    class ATM
        /// <summmary>
        /// This is the class for the ATM classes. Contains constructors + method functions for the ATM class.
        /// </summmary>

        {
            public Bank _Bank;

            /// <summary>
            /// This function constructs an ATM; it requires an Bank object to be passed to it. 
            /// </summary>
            /// <param name="ATM_BANK">This takes a bank parameter that the ATM class accesses. </param>
            public ATM(Bank ATM_BANK)
            {
            int SELECT_ACCOUNT_OPTION = 1;
            int CREATE_ACCOUNT_OPTION = 2;
            int EXIT_ACCOUNT_APPLICATION_OPTION = 3;

            int CHECK_BALANCE_OPTION = 1;
            int WITHDRAW_OPTION = 2;
            int DEPOSIT_OPTION = 3;
            int EXIT_ACCOUNT_OPTION = 4;
            _Bank = ATM_BANK;
            }
        }
    }

