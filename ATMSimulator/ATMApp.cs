﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMSimulator
{
    /// <summary>
    /// Class whichs runs the ATM Application.
    /// </summary>
    public class ATMApp
    {
        public Bank Sim_BANK;
        public ATM SIM_ATM;

        public ATMApp(Bank sim_BANK, ATM sIM_ATM)
        {
            Sim_BANK = sim_BANK;
            SIM_ATM = sIM_ATM;
        }

    }
}
