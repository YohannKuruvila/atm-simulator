﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMSimulator
{
    /// <summary>
    /// Creates an Bamk Account class with number, name, balance, and rate parameters.
    /// </summary>
    class Account
    {
        public int _accountNo;
        public string _accountName;
        public float _balance;
        public float _annualInterestRate = 0;
        public int ACCOUNT_TYPE_CHECKING = 1;
        public int ACCOUNT_TYPE_savings = 2;

        public Account(int AccountNo, string AccountName, float annualInterestRate)
        {
            _accountNo = AccountNo;
            _accountName = AccountName;
            _balance = 0;
            _annualInterestRate = annualInterestRate;
        }

        /// <summary>
        /// Getter for AccountNumber. 
        /// </summary>
        /// <returns>The Account Number as an integer.</returns>
        public int getAccountNumber()
        {
            return _accountNo;
        }


        /// <summary>
        /// Gets the account owner's name.
        /// </summary>
        /// <returns>A string containing the owners name.</returns>
        public string getAccountName()
        {
            return _accountName;
        }

        /// <summary>
        /// Getter for account balance.
        /// </summary>
        /// <returns> Float representation of the current bank balance.</returns>
        public float getBalance()
        {
            return _balance;
        }


        /// <summary>
        /// Gets Annual Interest Rate.
        /// </summary>
        /// <returns>The Annual Rate as a float</returns>
        public float getRate()
        {
            return _annualInterestRate;
        }


        /// <summary>
        /// Sets the new Annual Rate.
        /// </summary>
        /// <param name="NewRate">Any positive number.</param>
        public void setRate(float NewRate)
        {
            _annualInterestRate = (NewRate/100);
        }

        /// <summary>
        /// Getter for the monthly rate of interest.
        /// </summary>
        /// <returns>Float of mounthly interest.</returns>
        public float MonthlyRate()
        {
            return (_annualInterestRate / 12);
        }

        /// <summary>
        /// Deposit function that adds more money to the balance and checks that the amount is positive.
        /// </summary>
        /// <param name="amount">How much you want to add.</param>
        /// <returns>The new balance.</returns>
        public float Deposit(float amount)
        {
            if (amount < 100) {
                Console.WriteLine("Invalid amount.");
                return _balance;
            }
            else
            {
                float oldBalance = _balance;
                _balance += amount;

                return _balance;
            }
        }

        /// <summary>
        /// function that removes money from the bank account and ensures that a valid input and balance exist before taking away money.
        /// </summary>
        /// <param name="amount">How much is taken.</param>
        /// <returns>The new balance.</returns>
        public float Withdraw(float amount)
        {
            if (amount < 0)
            {
                Console.WriteLine("No Negative Removals.");
                return _balance;
            }

            else if (amount > _balance)
            {
                Console.WriteLine("Larger then existing balance");
                return _balance;
            }

            else
            {
                float OldBalance = _balance;
                _balance -= amount;
                return _balance;

            }
        }
        
    }
}
