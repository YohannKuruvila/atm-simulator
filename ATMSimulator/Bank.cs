﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMSimulator
{
    public class Bank
    {
        /// <summary>
        /// Constructs the Bank class; requires no parameters to be provided to it. Creates a list of accounts and the value that account numbers start at. 
        /// </summary>
        public Bank()
        {
            int DEFAUlT_ACCOUNT_NO_START = 100;
            List<Account> accountList = new List<Account>();
        }
        internal List<Account> AccountList { get; set; }
        }


        /// <summary>
        /// This function searches the account list to see if it can find an account with a matching number using the inbuilt Account class functionality. 
        /// </summary>
        /// <param name="AccountNo">The Account Number.</param>
        /// <param name="accountList">List of accounts at the bank.</param>
        /// <returns>Returns account if it exists inside the list.</returns>
        public Account FindAccount(int AccountNo, List<Account> accountList)
        {
            foreach (var accounts in accountList)
            {
                if (accounts.getAccountNumber() == AccountNo)
                {
                    return accounts;
                }
                else
                {
                    return null;
                }

            }

        }

        /// <summary>
        /// Function that sets bank account number by running a while loop and checking to see if a valid input or exit has been provided to it.
        /// </summary>
        /// <returns>Returns the bank account number if a valid one has been chosen.</returns>
        public int determineAccountNumber()
        {
            bool found = false;

            while (found == false)
            {
                Console.WriteLine("Please enter account number [100-1000] or press [ENTER] to cancel.)");
                string input = Console.ReadLine();
                if (input.Length == 0)
                {
                    Console.WriteLine("Exiting program.");
                    return 0;
                }
                int AcctNo = Int32.Parse(input);
                return AcctNo;

            }

        }
    }
}
